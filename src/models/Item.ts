export interface Item {
    name: string;
    category: string;
    providesDiscount: boolean;
    isInsurable: boolean;
    sentToAgent: boolean;
    value: number;
    location: Location;
    description: string;
    imageURL: string;
}

export interface Location {
    longitude: string;
    latitude: string;
    room: string;
}
