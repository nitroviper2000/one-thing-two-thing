import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Item} from "../../models/Item";
import { ItemDetailsPage } from "../item-details/item-details";
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { isArray } from "ionic-angular/util/util";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  public items : Array<Item>;

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public http: HttpClient) {


  }

  ionViewDidEnter() {
    console.log("i entered the list page!")
    this.items = itemsDb;
    var tempThis = this;
    this.storage.get('items').then(function(items){
      console.log("after storage get");
      console.log(items);
      if(!isArray(items)) {
        items = [];
      }
      tempThis.items =  tempThis.items.concat(items);
    })
  }

  private itemSelected(item: Item) {
    console.log("someone clicked" + item.name);
    this.navCtrl.setRoot(ItemDetailsPage, {item});
  }

  private submitToInsurer(items: Array<Item>) {
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    items.forEach((item) => {
      console.log("read to call server for " + JSON.stringify(item));
      return this.http.post('https://fathomless-plateau-74799.herokuapp.com/items', JSON.stringify(item), httpOptions).subscribe(data => {
      //TODO handle bad response codes (404, 500, etc...)
      });
    })
  }
}

var itemsDb = [
  {
     "name":"gold-colored ring",
     "category":"jewelry",
     "providesDiscount":false,
     "isInsurable":true,
     "sentToAgent":false,
     "value":1500,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542345/542345883/20180721_133419.jpg"
  },
  {
     "name":"blue gemstone silver-colored round cut ring",
     "category":"jewelry",
     "providesDiscount":false,
     "isInsurable":true,
     "sentToAgent":false,
     "value":1500,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542353/542353818/20180721_133422.jpg"
  },
  {
     "name":"round silver analog watch with link bracelet",
     "category":"jewelry",
     "providesDiscount":false,
     "isInsurable":true,
     "sentToAgent":false,
     "value":1500,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542354/542354533/20180721_133429.jpg"
  },
  {
     "name":"gray laptop computer",
     "category":"electronics",
     "providesDiscount":false,
     "isInsurable":true,
     "sentToAgent":false,
     "value":1000,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542398/542398468/20180721_133552.jpg"
  },
  {
     "name":"round white and red fire alarm",
     "category":"safety",
     "providesDiscount":true,
     "isInsurable":false,
     "sentToAgent":false,
     "value":10,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542398/542398898/20180721_133632.jpg"
  },
  {
     "name":"white smoke detector sprinkler",
     "category":"safety",
     "providesDiscount":true,
     "isInsurable":false,
     "sentToAgent":false,
     "value":10,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542399/542399190/20180721_133646.jpg"
  },
  {
     "name":"black and white musical instrument",
     "category":"instruments",
     "providesDiscount":false,
     "isInsurable":false,
     "sentToAgent":false,
     "value":500,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542399/542399440/20180721_134559.jpg"
  },
  {
     "name":"red and black city bike",
     "category":"bicycle",
     "providesDiscount":false,
     "isInsurable":true,
     "sentToAgent":false,
     "value":700,
     "location":{
        "longitude":"100",
        "latitude":"100",
        "room":"living"
     },
     "description":"",
     "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542400/542400093/20180721_145003.jpg"
  },
  {
    "name":"white Samsung Galaxy android smartphone",
    "category":"phones",
    "providesDiscount":false,
    "isInsurable":true,
    "sentToAgent":false,
    "value":500,
    "location":{
       "longitude":"100",
       "latitude":"100",
       "room":"living"
    },
    "description":"",
    "imageURL":"http://assets.cloudsight.ai/uploads/image_request/image/542/542408/542408110/20180721_151051.jpg"
 }
]
