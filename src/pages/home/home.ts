import { Component, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Item } from '../../models/Item';
import { isArray } from 'ionic-angular/util/util';
import { LoadingController } from 'ionic-angular';

const apiSettings = {
  "key": "fKrVzNwx7CexBfpyUMxtIQ",
  "baseUrl": "https://api.cloudsight.ai/v1/images"
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
  public image: File;
  //public debugString: string;
  //public debugString2: string;
  public gotImageAnalysis: EventEmitter<any> = new EventEmitter();
  public currentItem: Item;
  public loader;

  constructor(
    public navCtrl: NavController,
    public camera: Camera,
    public http: HttpClient,
    public storage: Storage,
    public loading: LoadingController
  ) {
    this.loader = this.loading.create({
      content: 'Analyzing image...',
    });
    this.gotImageAnalysis.subscribe((imageAnalysis)=> {
      this.addItem(imageAnalysis)
    });
  }

  takePicture() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 500,
      targetHeight: 500
    }).then((imageData) => {
      let formattedPicture = this.formatPicture(imageData);
      this.sendPictureForAnalysis(formattedPicture);
    }, (err) => {
      console.log(err);
    });
  }

  generateItem(imageAnalysis) {


    var newItem: Item;

    newItem = {
      name: imageAnalysis.name,
      category: 'Miscellaneous',
      providesDiscount: false,
      isInsurable: false,
      sentToAgent: false,
      value: 0,
      location: { longitude: '100', latitude: '100', room: 'living' },
      description: '',
      imageURL: imageAnalysis.url
    }

    let location: { longitude: '100', latitude: '100', room: 'living' };

    let categories = [
      {
        'categoryName': 'electronics',
        'itemsInCategory': ['laptop', 'heaphones', 'camera'],
        'insurable': true,
        'discountable': false,
        'averageValue': 1000
      },
      {
        'categoryName': 'jewelry',
        'itemsInCategory': ['ring', 'necklace', 'watch', 'gold', 'coin','gem','diamond'],
        'insurable': true,
        'discountable': false,
        'averageValue': 1500
      },
      {
        'categoryName': 'guns',
        'itemsInCategory': ['rifle', 'handgun'],
        'insurable': true,
        'discountable': false,
        'averageValue': 750
      },
      {
        'categoryName': 'phones',
        'itemsInCategory': ['phone', 'cellphone'],
        'insurable': true,
        'discountable': false,
        'averageValue': 500
      },
      {
        'categoryName': 'bicycle',
        'itemsInCategory': ['bicycle', 'bike'],
        'insurable': true,
        'discountable': false,
        'averageValue': 700
      },
      {
        'categoryName': 'instruments',
        'itemsInCategory': ['keyboard', 'guitar', 'piano', 'melodica', 'instrument'],
        'insurable': true,
        'discountable': false,
        'averageValue': 800
      },
      {
        'categoryName': 'dishware',
        'itemsInCategory': ['cup', 'mug', 'can', 'plate'],
        'insurable': false,
        'discountable': false,
        'averageValue': 10
      },
      {
        'categoryName': 'safety',
        'itemsInCategory': ['extinguisher', 'alarm', 'fire', 'sprinkler'],
        'insurable': false,
        'discountable': true,
        'averageValue': 10
      }
    ]



    categories.forEach((category) => {
      category.itemsInCategory.forEach((catItem) => {
        if(newItem.name.includes(catItem)){
          newItem.category = category.categoryName;
          newItem.isInsurable = category.insurable;
          newItem.providesDiscount = category.discountable;
          newItem.value = category.averageValue;
        }
      })
    })
    console.log('classified', newItem);
    return newItem;

  }
  

  addItem(imageAnalysis) {
    let newItem = this.generateItem(imageAnalysis);
    this.currentItem = newItem;
    console.log('currentItem', this.currentItem);
    var tempThis = this;
    this.storage.get('items').then(function (items) {
      console.log('got items', items);
      if (!isArray(items)) {
        items = [];
        items.push(newItem);

      } else {
        items.push(newItem);
      }
      tempThis.storage.set('items', items).then(function (items) {
        console.log('storage set!', items);
      })
    })

  }

  handleFileInput(files: FileList) {
    let fileToUpload = files.item(0);
    this.image = fileToUpload;
    this.sendPictureForAnalysis(fileToUpload);
  }

  sendPictureForAnalysis(image) {

    var httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'CloudSight ' + apiSettings.key,
      })
    };

    let formData: FormData = new FormData();
    formData.append('image', image, image.name);
    formData.append('locale', 'en_US');

    var tempThis = this;
    return this.http.post(apiSettings.baseUrl, formData, httpOptions).subscribe(data => {
      var dataTemp: any = data;
      if (dataTemp.status != 'skipped') {
        this.isPictureReady(dataTemp.token);
      }
    });
  }

  isPictureReady(token) {
    console.log('is picture ready?', token);
    var httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'CloudSight ' + apiSettings.key,
      })
    };
    var tempThis = this;
    return this.http.get(apiSettings.baseUrl + '/' + token, httpOptions).subscribe(data => {
      var dataTemp: any = data;
      if (dataTemp.status == 'completed') {
        tempThis.gotImageAnalysis.emit(data);
      } else {
        setTimeout(function () {
          tempThis.isPictureReady(dataTemp.token);
        }, 1000);
      }
    })
  }

  formatPicture(imageData) {
    var fr = new FileReader();
    var tempImage = "data:image/jpeg;base64," + imageData;
    return this.dataURLtoFile(tempImage, 'bob');
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }



}
